<div class="row justify-content-center" style="margin-bottom:50px;">
    <div class="col text-center">
        <img class="img-header" src="./images/municipios/TODO EL ESTADO.png" alt="Mapa Hidalgo">
    </div>
    <div class="col-6 align-self-center txt">
        <h2 class="text-center subtitle">
            REPORTE GENERAL - SISTEMAS
        </h2>
    </div>
    <div class="col text-center">
        <img class="img-header" src="./images/pri.png" alt="NIP">
    </div>
</div>

<div class="row justify-content-center" style="margin-bottom:55px;">
    <div class="col-12 col-lg-10">
        <div class="table-responsive">
            <table class="table table-hover text-center">
                <thead class="thead-dark">
                    <tr>
                        <th>SISTEMA</th>
                        <th>META</th>
                        <th>AVANCE</th>
                        <th>PENDIENTE</th>
                        <th>PORCENTAJE</th>
                    </tr>
                </thead>
                <tbody id="tablemaster"></tbody>
            </table>
        </div>
    </div>
</div>

<div class="row justify-content-center" style="margin-bottom:85px;">
    <div class="col-12 col-lg-6">
        <canvas id="protectoraChart" style="width:100% !important; height: 350px !important;"></canvas>
    </div>
    <div class="col-12 col-lg-6">
        <canvas id="amigosChart" style="width:100% !important; height: 350px !important;"></canvas>
    </div>
</div>

<script src="./js/sise/data-master.js"></script>