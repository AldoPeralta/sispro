<?php
    session_start();
    if (!isset($_SESSION['auth'])){
        session_destroy();
  	    header ("Location: /index.php");
    }
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css">
        <script src="https://cdn.jsdelivr.net/npm/sweetalert2@10"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.9.3/Chart.min.js"></script>
        <script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
        <script src="https://kit.fontawesome.com/2c36e9b7b1.js" crossorigin="anonymous"></script>
        <link rel="stylesheet" href="./css/main.css">
        <link rel="stylesheet" href="./css/select.css">
        <link rel="icon" href="./favicon.ico">
        <title>SISPRO</title>
    </head>
    <body>
        <div class="cover-container d-flex w-100 h-100 mx-auto flex-column">
            <header class="masthead">
                <div class="inner">
                    <h3 class="masthead-brand"><b>SISPRO</b></h3>
                    <nav class="nav nav-masthead justify-content-center">
                        <?php
                            if($_SESSION["nivel"] != "3"){
                                echo '<a class="nav-link active" href="dashboard.php">Reporte General</a>';
                            }
                            if($_SESSION["nivel"] == "2"){
                                echo '<a class="nav-link active" href="promotor.php">Reporte Promotor</a>';
                            }
                        ?>
                        <a class="nav-link active" href="logout.php">Cerrar Sesión</a>
                    </nav>
                </div>
            </header>