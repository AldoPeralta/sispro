<?php require 'onHead.php';?>

    <div class="container">
        <div class="row justify-content-center mb-3">
            <div class="col-12 col-lg-6">
                <h1 class="text-center title">
                    BIENVENIDO 
                    <span class="badge badge-danger">
                        <?php echo $_SESSION["user"];?>
                    </span>
                </h1>
            </div>
        </div>

        <?php
            if ($_SESSION["nivel"] == "1") {
                require "administrador.php";
            } else if($_SESSION["nivel"] == "2"){
                require "municipal.php";
            } else if($_SESSION["nivel"] == "3"){
                require "punteo.php";
            } else if($_SESSION["nivel"] == "0"){
                require "master.php";
            }
        ?>

    </div>

<?php require 'onBody.php';?>