const promotores = document.getElementById('promotores');
const btnCargar = document.getElementById('btnCargar');

const updateEstatus = (event) => {
    var data = event.target.id.split("-");
    document.getElementById(`row-${data[1]}`).remove();
    if(localStorage.getItem("pendientes") === null){
        var punteos = [{
            cve: data[1],
            estatus: data[2]
        }];
        localStorage.setItem("pendientes",JSON.stringify(punteos));
        document.getElementById('punteos').innerText = `${punteos.length}`;
    } else{
        var punteos = JSON.parse(localStorage.getItem("pendientes"));
        punteos.push({
            cve: data[1],
            estatus: data[2]
        });
        localStorage.setItem("pendientes",JSON.stringify(punteos));
        document.getElementById('punteos').innerText = `${punteos.length}`;
    }
}

const sendData = event => {
    if(localStorage.getItem("pendientes") !== null){
        var request = new XMLHttpRequest();
        request.open('POST', 'private/data-punteo.php');

        var data = new FormData();
        data.append("data",localStorage.getItem("pendientes"));

        request.onload = function(){
            var result = JSON.parse(request.responseText);
            if(result.success){
                Swal.fire({
                    title: 'Datos Guardados Correctamente',
                    icon: 'success',
                    html: 'Espere un momento por favor...',
                    timer: 1000,
                    timerProgressBar: true,
                    onBeforeOpen: () => {
                        Swal.showLoading();
                    },
                    onClose: () => {
                        window.location.reload();
                    }
                });
            } else{
                Swal.fire({
                    title: 'Datos Guardados Correctamente',
                    icon: 'error',
                    html: 'Espere un momento por favor...',
                    timer: 2000,
                    timerProgressBar: true,
                    onBeforeOpen: () => {
                        Swal.showLoading();
                    },
                    onClose: () => {
                        window.location.reload();
                    }
                });
            }
        }

        request.onreadystatechange = function(){
            if(request.readyState == 2 && request.status == 200){
                Swal.fire({
                    title: 'Obteniendo Datos',
                    html: 'Espere un momento por favor...',
                    timer: 1000,
                    timerProgressBar: true,
                    onBeforeOpen: () => {
                        Swal.showLoading();
                    }
                });
            }
        }

        request.send(data);
    }
}

document.addEventListener('DOMContentLoaded', () => {

    localStorage.removeItem("pendientes");

    btnCargar.addEventListener('click', sendData);
    
    var request = new XMLHttpRequest();
    request.open('POST', 'private/data-punteo.php');

    var data = new FormData();
    data.append("id",localStorage.getItem("idUser"));

    request.onload = function(){
        var result = JSON.parse(request.responseText);
        if(result){
            result.forEach(promotor => {
                var option = document.createElement('option');
                var name = document.createTextNode(promotor.nombre + " (" + promotor.pendientes + " Pendientes)" );
                option.setAttribute("value",promotor.id);
                option.appendChild(name);
                promotores.appendChild(option);
            });
        } else{
            Swal.fire({
                icon: 'error',
                title: 'Eror de Busqueda',
                text: 'No es posible conectar con la base de datos intentelo mas tarde'
            });
        }
    }

    request.onreadystatechange = function(){
        if(request.readyState == 2 && request.status == 200){
            Swal.fire({
                title: 'Obteniendo Datos',
                html: 'Espere un momento por favor...',
                timer: 1000,
                timerProgressBar: true,
                onBeforeOpen: () => {
                    Swal.showLoading();
                }
            });
        }
    }

    request.send(data);
});

promotores.addEventListener('change', event => {
    var request = new XMLHttpRequest();
    request.open('POST', 'private/data-punteo.php');
    var data = new FormData();
    data.append("promotor", event.target.value);

    request.onload = function(){
        var result = JSON.parse(request.responseText);
        if(result){
            document.getElementById('tablepromov').innerHTML = '';
            document.getElementById('tablenopromov').innerHTML = '';
            var promov = 0, nopromov = 0;

            result.forEach(row => {
                var fila = document.createElement('tr');
                fila.setAttribute("id",`row-${row.id}`);
                fila.innerHTML += ('<td class="text-center">' + row.nombre + '</td>');
                fila.innerHTML += ('<td class="text-center">' + row.edad + '</td>');
                if(parseInt(row.estatus) !== 1){
                    nopromov++;
                    fila.innerHTML += (`<td class="rowdata"><span class="btn btn-success w-100"><i class="fas fa-check" id="btn-${row.id}-1"></i></span></td>`);
                    document.getElementById('tablenopromov').appendChild(fila);
                } else{
                    promov++;
                    fila.innerHTML += (`<td class="rowdata"><span class="btn btn-danger w-100"><i class="fas fa-times" id="btn-${row.id}-0"></i></span></td>`);
                    document.getElementById('tablepromov').appendChild(fila);
                }
            });

            document.getElementById('btnPromov').innerHTML = `<b>ASISTENCIA (${promov})</b> <i class="fas fa-arrow-down"></i>`;
            document.getElementById('btnNoPromov').innerHTML = `<b>PENDIENTES (${nopromov})</b> <i class="fas fa-arrow-down"></i>`;

            const buttons = document.querySelectorAll('.rowdata span i');
            buttons.forEach( button => {
                button.addEventListener('click', updateEstatus);
            });
        } else{
            Swal.fire({
                icon: 'error',
                title: 'Error de Búsqueda',
                text: 'No es posible conectar con la base de datos inténtelo mas tarde'
            });
        }
    }

    request.onreadystatechange = function(){
        if(request.readyState == 2 && request.status == 200){
            Swal.fire({
                title: 'Obteniendo Datos',
                html: 'Espere un momento por favor...',
                timer: 1000,
                timerProgressBar: true,
                onBeforeOpen: () => {
                    Swal.showLoading();
                }
            });
        }
    }

    request.send(data);
});