const getDataArea = (event) => {
    var id = event.target.id.split("-")[1];
    var nombre = document.getElementById(event.target.id).innerText;
    localStorage.setItem("idArea",id);
    localStorage.setItem("nombreArea",nombre);
    window.location.href = "municipaladmin.php";
}

document.addEventListener('DOMContentLoaded', () => {

    var request = new XMLHttpRequest();
    request.open('POST','private/data-admin.php');

    request.onload = function(){
        var result = JSON.parse(request.responseText);
        var areas = [], avances =[], pendientes =[];
        if(result.success){
            result.areas.forEach(row => {
                var fila = document.createElement('tr');
                fila.innerHTML += (`<td class="rowarea"><button class="btn btn-secondary w-100" id="area-${row.id}">` + row.area + '</button></td>');
                fila.innerHTML += ('<td>' + row.listado + '</td>');
                fila.innerHTML += ('<td>' + row.avance + '</td>');
                fila.innerHTML += ('<td>' + row.pendiente + '</td>');
                fila.innerHTML += ('<td><b>' + row.porcentaje + '%</b></td>');
                document.getElementById('tableadmin').appendChild(fila);
                areas.push(row.area);
                row.avance = row.avance.replace(",","");
                row.pendiente = row.pendiente.replace(",","");
                avances.push(parseInt(row.avance));
                pendientes.push(parseInt(row.pendiente));
            });

            const buttons = document.querySelectorAll('.rowarea button');
            buttons.forEach( button => {
                button.addEventListener('click', getDataArea);
            });

            var fila = document.createElement('tr');
            fila.innerHTML += ('<td class="text-center"><b>TOTAL:</b></td>');
            fila.innerHTML += ('<td><b>' + result.totales.total_listado + '</b></td>');
            fila.innerHTML += ('<td><b>' + result.totales.total_avance + '</b></td>');
            fila.innerHTML += ('<td><b>' + result.totales.total_pendiente + '</b></td>');
            fila.innerHTML += (`<td><b>${result.totales.total_porcentaje}%</b></td>`);
            document.getElementById('tableadmin').appendChild(fila);

            var ctx = document.getElementById('adminChart');
            var myChart = new Chart(ctx, {
                type: 'bar',
                data: {
                    labels: areas,
                    datasets: [
                        {
                            label: 'Avance',
                            data: avances,
                            backgroundColor: '#28A745',
                            borderColor: '#28A745',
                            borderWidth: 1
                        },
                        {
                            label: 'Pendiente',
                            data: pendientes,
                            backgroundColor: "#DC3545",
                            borderColor: "#DC3545",
                            borderWidth: 1,
                        }
                    ]
                },
                options: {
                    responsive: true,
                    legend: {
                        position: 'top'
                    },
                    title: {
                        display: true,
                        text: "Estadisticas Por Área"
                    }
                }
            });
        } else{
            Swal.fire({
                icon: 'error',
                title: 'Eror de Busqueda',
                text: 'No es posible conectar con la base de datos intentelo mas tarde'
            });
        }
    }

    request.onreadystatechange = function(){
        if(request.readyState == 2 && request.status == 200){
            Swal.fire({
                title: 'Obteniendo Datos',
                html: 'Espere un momento por favor...',
                timer: 1000,
                timerProgressBar: true,
                onBeforeOpen: () => {
                    Swal.showLoading();
                }
            });
        }
    }

    request.send();
});