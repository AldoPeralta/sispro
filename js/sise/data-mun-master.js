const showMunData = (event) => {
    var id = event.target.id.split("-")[1];
    Swal.fire({
        title: 'Candidato',
        text: document.getElementById(event.target.id).innerText,
        imageUrl: `../../images/${id}.jpg`,
        imageWidth: 400,
        imageHeight: 200,
        imageAlt: 'Candidato',
    });
}

document.addEventListener('DOMContentLoaded', () => {

    document.getElementById("area").innerText = localStorage.getItem("nombreSistema");

    var request = new XMLHttpRequest();
    request.open('POST','private/data-mun-master.php');

    var data = new FormData();
    data.append("sistema",localStorage.getItem("idSistema"));

    request.onload = function(){
        var result = JSON.parse(request.responseText);
        if(result.success){
            var id = null;
            result.municipios.forEach(row => {
                id = row.id
                var fila = document.createElement('tr');
                fila.innerHTML += (`<td class="rowmun"><button class="btn btn-secondary w-100" id="mun-${row.id}">` + row.municipio + '</button></td>');
                fila.innerHTML += ('<td>' + row.listado + '</td>');
                fila.innerHTML += ('<td>' + row.avance + '</td>');
                fila.innerHTML += ('<td>' + row.pendiente + '</td>');
                fila.innerHTML += ('<td><b>' + row.porcentaje + '%</b></td>');
                document.getElementById('tablemunmaster').appendChild(fila);
            });

            const buttons = document.querySelectorAll('.rowmun button');
            buttons.forEach( button => {
                button.addEventListener('click', showMunData);
            });

            var fila = document.createElement('tr');
            fila.innerHTML += ('<td class="text-center"><b>TOTAL:</b></td>');
            fila.innerHTML += ('<td><b>' + result.totales.total_listado + '</b></td>');
            fila.innerHTML += ('<td><b>' + result.totales.total_avance + '</b></td>');
            fila.innerHTML += ('<td><b>' + result.totales.total_pendiente + '</b></td>');
            fila.innerHTML += (`<td><b>${result.totales.total_porcentaje}%</b></td>`);
            document.getElementById('tablemunmaster').appendChild(fila);

            var ctx = document.getElementById('municipalMasterChart');
            var myChart = new Chart(ctx, {
                type: 'pie',
                data: {
                    labels: ['Listados','No Listados'],
                    datasets: [{
                        label: 'Punteados',
                        data: [parseInt(result.avance),parseInt(result.pendiente)],
                        backgroundColor: ['rgba(40, 167, 69)','rgba(220, 53, 69)'],
                        borderColor: ['rgba(0, 255, 0)','rgba(220, 53, 69)'],
                        borderWidth: 1,
                    }]
                },
                options: { scales: { y: { beginAtZero: true } } }
            });
        } else{
            Swal.fire({
                icon: 'error',
                title: 'Eror de Busqueda',
                text: 'No es posible conectar con la base de datos intentelo mas tarde'
            });
        }
    }

    request.onreadystatechange = function(){
        if(request.readyState == 2 && request.status == 200){
            Swal.fire({
                title: 'Obteniendo Datos',
                html: 'Espere un momento por favor...',
                timer: 1000,
                timerProgressBar: true,
                onBeforeOpen: () => {
                    Swal.showLoading();
                }
            });
        }
    }

    request.send(data);
});