const select = document.querySelector('#select');
const opciones = document.querySelector('#opciones');
const contenidoSelect = document.querySelector('#select .contenido-select');
const hiddenInput = document.querySelector('#inputSelect');
const btnCargar = document.getElementById('btnCargar');

const updateEstatus = (event) => {

    var data = event.target.id.split("-");
    document.getElementById(`row-${data[1]}`).remove();
    if(localStorage.getItem("pendientes") === null){
        var punteos = [{
            cve: data[1],
            estatus: data[2]
        }];
        localStorage.setItem("pendientes",JSON.stringify(punteos));
        document.getElementById('punteos').innerText = `${punteos.length}`;
    } else{
        var punteos = JSON.parse(localStorage.getItem("pendientes"));
        punteos.push({
            cve: data[1],
            estatus: data[2]
        });
        localStorage.setItem("pendientes",JSON.stringify(punteos));
        document.getElementById('punteos').innerText = `${punteos.length}`;
    }
}

const sendData = event => {
    if(localStorage.getItem("pendientes") !== null){
        var request = new XMLHttpRequest();
        request.open('POST', 'private/data-punteo.php');

        var data = new FormData();
        data.append("data",localStorage.getItem("pendientes"));

        request.onload = function(){
            var result = JSON.parse(request.responseText);
            if(result.success){
                Swal.fire({
                    title: 'Datos Guardados Correctamente',
                    icon: 'success',
                    html: 'Espere un momento por favor...',
                    timer: 1000,
                    timerProgressBar: true,
                    onBeforeOpen: () => {
                        Swal.showLoading();
                    },
                    onClose: () => {
                        window.location.reload();
                    }
                });
            } else{
                Swal.fire({
                    title: 'Datos Guardados Correctamente',
                    icon: 'error',
                    html: 'Espere un momento por favor...',
                    timer: 2000,
                    timerProgressBar: true,
                    onBeforeOpen: () => {
                        Swal.showLoading();
                    },
                    onClose: () => {
                        window.location.reload();
                    }
                });
            }
        }

        request.onreadystatechange = function(){
            if(request.readyState == 2 && request.status == 200){
                Swal.fire({
                    title: 'Obteniendo Datos',
                    html: 'Espere un momento por favor...',
                    timer: 1000,
                    timerProgressBar: true,
                    onBeforeOpen: () => {
                        Swal.showLoading();
                    }
                });
            }
        }

        request.send(data);
    }
}

const clickPromotor = event => {

    contenidoSelect.innerHTML = event.currentTarget.innerHTML;
    select.classList.toggle('active');
    opciones.classList.toggle('active');

    var id = event.currentTarget.id.split("-");

    var request = new XMLHttpRequest();
    request.open('POST', 'private/data-punteo.php');

    var data = new FormData();
    data.append("promotor", id[1]);

    request.onload = function(){
        var result = JSON.parse(request.responseText);
        if(result){
            document.getElementById('tablepromov').innerHTML = '';
            document.getElementById('tablenopromov').innerHTML = '';
            var promov = 0, nopromov = 0;

            result.forEach(row => {
                var fila = document.createElement('tr');
                fila.setAttribute("id",`row-${row.id}`);
                fila.innerHTML += ('<td class="text-center">' + row.nombre + '</td>');
                fila.innerHTML += ('<td class="text-center">' + row.edad + '</td>');
                if(parseInt(row.estatus) !== 1){
                    nopromov++;
                    fila.innerHTML += (`<td class="rowdata"><span class="btn btn-success w-100"><i class="fas fa-check" id="btn-${row.id}-1"></i></span></td>`);
                    document.getElementById('tablenopromov').appendChild(fila);
                } else{
                    promov++;
                    fila.innerHTML += (`<td class="rowdata"><span class="btn btn-danger w-100"><i class="fas fa-times" id="btn-${row.id}-0"></i></span></td>`);
                    document.getElementById('tablepromov').appendChild(fila);
                }
            });

            document.getElementById('btnPromov').innerHTML = `<b>ASISTENCIA (${promov})</b> <i class="fas fa-arrow-down"></i>`;
            document.getElementById('btnNoPromov').innerHTML = `<b>PENDIENTES (${nopromov})</b> <i class="fas fa-arrow-down"></i>`;

            const buttons = document.querySelectorAll('.rowdata span i');
            buttons.forEach( button => {
                button.addEventListener('click', updateEstatus);
            });
        } else{
            Swal.fire({
                icon: 'error',
                title: 'Error de Búsqueda',
                text: 'No es posible conectar con la base de datos inténtelo mas tarde'
            });
        }
    }

    request.onreadystatechange = function(){
        if(request.readyState == 2 && request.status == 200){
            Swal.fire({
                title: 'Obteniendo Datos',
                html: 'Espere un momento por favor...',
                timer: 1000,
                timerProgressBar: true,
                onBeforeOpen: () => {
                    Swal.showLoading();
                }
            });
        }
    }

    request.send(data);
}

document.addEventListener('DOMContentLoaded', () => {

    select.addEventListener('click', () => {
        select.classList.toggle('active');
        opciones.classList.toggle('active');
    });

    localStorage.removeItem("pendientes");
    btnCargar.addEventListener('click', sendData);

    var request = new XMLHttpRequest();
    request.open('POST', 'private/data-punteo.php');

    var data = new FormData();
    data.append("id",localStorage.getItem("idUser"));

    request.onload = function(){
        var result = JSON.parse(request.responseText);
        if(result){
            result.forEach(promotor => {
                var option = document.createElement('a')
                option.classList.add('opcion');
                var div = document.createElement('div')
                div.classList.add('contenido-opcion');
                var texto = document.createElement('div')
                texto.classList.add('textos');
                var titulo = document.createElement('h4')
                titulo.classList.add('tituloC');
                var descripcion = document.createElement('p')
                descripcion.classList.add('descripcion');
                titulo.innerHTML = promotor.nombre + "<br> <b>(" + promotor.pendientes + " Pendientes)</b>";
                descripcion.innerText = promotor.telefono;
                texto.appendChild(titulo);
                texto.appendChild(descripcion);
                div.appendChild(texto);
                option.appendChild(div);
                opciones.appendChild(option);
                option.setAttribute('id',`pro-${promotor.id}`);
            });

            document.querySelectorAll('#opciones > .opcion').forEach((opcion) => {
                opcion.addEventListener('click', clickPromotor);
            });

        } else{
            Swal.fire({
                icon: 'error',
                title: 'Eror de Busqueda',
                text: 'No es posible conectar con la base de datos intentelo mas tarde'
            });
        }
    }

    request.onreadystatechange = function(){
        if(request.readyState == 2 && request.status == 200){
            Swal.fire({
                title: 'Obteniendo Datos',
                html: 'Espere un momento por favor...',
                timer: 1000,
                timerProgressBar: true,
                onBeforeOpen: () => {
                    Swal.showLoading();
                }
            });
        }
    }

    request.send(data);
});