document.addEventListener('DOMContentLoaded', () => {
    var municipio = document.getElementById('titulo').innerText;

    var data = new FormData();
    data.append('munsecc', municipio);
    var request = new XMLHttpRequest();
    request.open('POST','data/data-seccion.php');

    request.onload = function(){
        var result = JSON.parse(request.responseText);
        if(result.success){

            result.municipios.forEach(row => {
                var fila = document.createElement('tr');
                fila.innerHTML += ('<td>' + row.municipio + '</td>');
                fila.innerHTML += ('<td>' + row.seccion + '</td>');
                fila.innerHTML += ('<td>' + row.listado + '</td>');
                fila.innerHTML += ('<td>' + row.meta + '</td>');
                fila.innerHTML += ('<td>' + row.avance + '</td>');
                fila.innerHTML += ('<td>' + row.pendiente + '</td>');
                fila.innerHTML += ('<td>' + row.porcentaje + '%</td>');
                document.getElementById('tablesecc').appendChild(fila);
            });

            var fila = document.createElement('tr');
            fila.innerHTML += ('<td></td>');
            fila.innerHTML += ('<td class="text-center"><b>TOTAL:</b></td>');
            fila.innerHTML += ('<td><b>' + result.totales.total_listado + '</b></td>');
            fila.innerHTML += ('<td><b>' + result.totales.total_meta + '</b></td>');
            fila.innerHTML += ('<td><b>' + result.totales.total_avance + '</b></td>');
            fila.innerHTML += ('<td><b>' + result.totales.total_pendiente + '</b></td>');
            fila.innerHTML += ('<td><b>' + result.totales.porcentaje_final + '%</b></td>');
            document.getElementById('tablesecc').appendChild(fila);

            var ctx = document.getElementById('seccChart');
            var myChart = new Chart(ctx, {
                type: 'pie',
                data: {
                    labels: ['Listados','No Listados'],
                    datasets: [{
                        label: 'Punteados',
                        data: [parseInt(result.avance),parseInt(result.pendiente)],
                        backgroundColor: ['rgba(40, 167, 69)','rgba(220, 53, 69)'],
                        borderColor: ['rgba(0, 255, 0)','rgba(220, 53, 69)'],
                        borderWidth: 1,
                    }]
                },
                options: { scales: { y: { beginAtZero: true } } }
            });
        } else{
            Swal.fire({
                icon: 'error',
                title: 'Eror de Busqueda',
                text: 'No es posible conectar con la base de datos intentelo mas tarde'
            });
        }
    }

    request.onreadystatechange = function(){
        if(request.readyState == 2 && request.status == 200){
            Swal.fire({
                title: 'Obteniendo Datos',
                html: 'Espere un momento por favor...',
                timer: 1000,
                timerProgressBar: true,
                onBeforeOpen: () => {
                    Swal.showLoading();
                }
            });
        }
    }

    request.send(data);
});