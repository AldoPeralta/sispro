var btn_submit = document.getElementById('senderbtn');
const formulario = document.getElementById('formulario');
const inputs = document.querySelectorAll('#formulario input');

const expresiones = {
	usuario: /^[a-zA-Z0-9-]{4,16}$/,
	password: /^.{6,16}$/
}

const campos = {
    usuario: false,
    password: false
}

const validarFormulario = (e) => {
    switch (e.target.name) {
        case "usuario":
			validarCampo(expresiones.usuario, e.target, 'usuario');
        break;
        case "password":
			validarCampo(expresiones.password, e.target, 'password');
        break;
    }
}

const validarCampo = (expresion,input,campo) => {
    if(expresion.test(input.value)){
		document.getElementById(`grupo__${campo}`).classList.remove('formulario__grupo-incorrecto');
		document.getElementById(`grupo__${campo}`).classList.add('formulario__grupo-correcto');
		document.querySelector(`#grupo__${campo} i`).classList.add('fa-check-circle');
		document.querySelector(`#grupo__${campo} i`).classList.remove('fa-times-circle');
		document.querySelector(`#grupo__${campo} .formulario__input-error`).classList.remove('formulario__input-error-activo');
		campos[campo] = true;
	} else {
		document.getElementById(`grupo__${campo}`).classList.add('formulario__grupo-incorrecto');
		document.getElementById(`grupo__${campo}`).classList.remove('formulario__grupo-correcto');
		document.querySelector(`#grupo__${campo} i`).classList.add('fa-times-circle');
		document.querySelector(`#grupo__${campo} i`).classList.remove('fa-check-circle');
		document.querySelector(`#grupo__${campo} .formulario__input-error`).classList.add('formulario__input-error-activo');
		campos[campo] = false;
	}
}

inputs.forEach((input) => {
    input.addEventListener('keyup', validarFormulario);
    input.addEventListener('blur', validarFormulario);
});

formulario.addEventListener('submit', (e) => {
    e.preventDefault();

    if(campos.usuario && campos.password){
        var request = new XMLHttpRequest();
        request.open('POST', 'private/login.php');

        var data = new FormData();
        data.append("usuario",document.getElementById("usuario").value);
        data.append("password",document.getElementById("password").value);

        request.onload = function(){
            console.log(request.responseText);
            var result = JSON.parse(request.responseText);
            if(result.success){
                localStorage.setItem("idUser", result.id);
                localStorage.setItem("idArea", result.area);
                window.location.href = "dashboard.php";
            } else if(result.nouser){
                Swal.fire({
                    icon: 'error',
                    title: 'Datos Incorrectos',
                    text: 'Usuario y/o contraseña incorrectos'
                });
            } else{
                Swal.fire({
                    icon: 'error',
                    title: 'Eror de Busqueda',
                    text: 'No es posible conectar con la base de datos intentelo mas tarde'
                });
            }
        }

        request.onreadystatechange = function(){
            if(request.readyState == 2 && request.status == 200){
                Swal.fire({
                    type: 'info',
                    title: 'Espere un momento',
                    text: 'Estamos validando sus datos'
                });
            }
        }

        request.send(data);
    }
});