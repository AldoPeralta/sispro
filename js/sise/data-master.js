const getDataArea = (event) => {
    var id = event.target.id;
    var nombre = document.getElementById(event.target.id).innerText;
    localStorage.setItem("idSistema",id);
    localStorage.setItem("nombreSistema",nombre);
    window.location.href = "municipalmaster.php";
}

document.addEventListener('DOMContentLoaded', () => {

    var request = new XMLHttpRequest();
    request.open('POST','private/data-master.php');

    request.onload = function(){
        var result = JSON.parse(request.responseText);
        if(result.success){
            var protectora = document.createElement('tr');
            protectora.innerHTML += (`<td class="areabtn"><button id="${'1'}" class="btn btn-secondary w-100">` + result.protectora.area + '</button></td>');
            protectora.innerHTML += ('<td>' + result.protectora.meta + '</td>');
            protectora.innerHTML += ('<td>' + result.protectora.avance + '</td>');
            protectora.innerHTML += ('<td>' + result.protectora.pendiente + '</td>');
            protectora.innerHTML += ('<td><b>' + result.protectora.porcentaje + '</b></td>');
            document.getElementById('tablemaster').appendChild(protectora);

            var amigos = document.createElement('tr');
            amigos.innerHTML += (`<td class="areabtn"><button id="${'2'}" class="btn btn-secondary w-100">` + result.amigos.area + '</button></td>');
            amigos.innerHTML += ('<td>' + result.amigos.meta + '</td>');
            amigos.innerHTML += ('<td>' + result.amigos.avance + '</td>');
            amigos.innerHTML += ('<td>' + result.amigos.pendiente + '</td>');
            amigos.innerHTML += ('<td><b>' + result.amigos.porcentaje + '</b></td>');
            document.getElementById('tablemaster').appendChild(amigos);

            const buttons = document.querySelectorAll('.areabtn button');
            buttons.forEach( button => {
                button.addEventListener('click', getDataArea);
            });

            var prote = document.getElementById('protectoraChart');
            var proteC = new Chart(prote, {
                type: 'pie',
                data: {
                    labels: ['Avance','Pendiente'],
                    datasets: [{
                        label: 'Reporte Protectora',
                        data: [parseInt(result.protectora.total_avance),parseInt(result.protectora.total_pendiente)],
                        backgroundColor: ['rgba(40, 167, 69)','rgba(220, 53, 69)'],
                        borderColor: ['rgba(0, 255, 0)','rgba(220, 53, 69)'],
                        borderWidth: 1,
                    }]
                },
                options: { 
                    scales: { y: { beginAtZero: true } },
                    title: { 
                        display: true,
                        text: 'Reporte Avance Protectora'
                    },
                    responsive: true,
                    maintainAspectRatio: false
                }
            });

            var amigos = document.getElementById('amigosChart');
            var amigosC = new Chart(amigos, {
                type: 'pie',
                data: {
                    labels: ['Avance','Pendiente'],
                    datasets: [{
                        label: 'Reporte Amigos',
                        data: [parseInt(result.amigos.total_avance),parseInt(result.amigos.total_pendiente)],
                        backgroundColor: ['rgba(40, 167, 69)','rgba(220, 53, 69)'],
                        borderColor: ['rgba(0, 255, 0)','rgba(220, 53, 69)'],
                        borderWidth: 1,
                    }]
                },
                options: { 
                    scales: { y: { beginAtZero: true } },
                    title: { 
                        display: true,
                        text: 'Reporte Avance Amigos'
                    },
                    responsive: true,
                    maintainAspectRatio: false
                }
            });
        } else{
            Swal.fire({
                icon: 'error',
                title: 'Eror de Busqueda',
                text: 'No es posible conectar con la base de datos intentelo mas tarde'
            });
        }
    }

    request.onreadystatechange = function(){
        if(request.readyState == 2 && request.status == 200){
            Swal.fire({
                title: 'Obteniendo Datos',
                html: 'Espere un momento por favor...',
                timer: 1000,
                timerProgressBar: true,
                onBeforeOpen: () => {
                    Swal.showLoading();
                }
            });
        }
    }

    request.send();
});