document.addEventListener('DOMContentLoaded', () => {

    document.getElementById("promotor").innerText = localStorage.getItem("nombreArea");

    var request = new XMLHttpRequest();
    request.open('POST','private/data-promotor.php');

    var data = new FormData();
    data.append("area",localStorage.getItem("idArea"));

    request.onload = function(){
        var result = JSON.parse(request.responseText);
        if(result.success){
            result.promotores.forEach(row => {
                var fila = document.createElement('tr');
                fila.innerHTML += ('<td>' + row.promotor + '</td>');
                fila.innerHTML += ('<td>' + row.listado + '</td>');
                fila.innerHTML += ('<td>' + row.avance + '</td>');
                fila.innerHTML += ('<td>' + row.pendiente + '</td>');
                fila.innerHTML += ('<td><b>' + row.porcentaje + '%</b></td>');
                document.getElementById('tablepromotor').appendChild(fila);
            });

            var fila = document.createElement('tr');
            fila.innerHTML += ('<td class="text-center"><b>TOTAL:</b></td>');
            fila.innerHTML += ('<td><b>' + result.totales.total_listado + '</b></td>');
            fila.innerHTML += ('<td><b>' + result.totales.total_avance + '</b></td>');
            fila.innerHTML += ('<td><b>' + result.totales.total_pendiente + '</b></td>');
            fila.innerHTML += (`<td><b>${result.totales.total_porcentaje}%</b></td>`);
            document.getElementById('tablepromotor').appendChild(fila);

            var ctx = document.getElementById('promotorChart');
            var myChart = new Chart(ctx, {
                type: 'pie',
                data: {
                    labels: ['Listados','No Listados'],
                    datasets: [{
                        label: 'Punteados',
                        data: [parseInt(result.avance),parseInt(result.pendiente)],
                        backgroundColor: ['rgba(40, 167, 69)','rgba(220, 53, 69)'],
                        borderColor: ['rgba(0, 255, 0)','rgba(220, 53, 69)'],
                        borderWidth: 1,
                    }]
                },
                options: { scales: { y: { beginAtZero: true } } }
            });
        } else{
            Swal.fire({
                icon: 'error',
                title: 'Eror de Busqueda',
                text: 'No es posible conectar con la base de datos intentelo mas tarde'
            });
        }
    }

    request.onreadystatechange = function(){
        if(request.readyState == 2 && request.status == 200){
            Swal.fire({
                title: 'Obteniendo Datos',
                html: 'Espere un momento por favor...',
                timer: 1000,
                timerProgressBar: true,
                onBeforeOpen: () => {
                    Swal.showLoading();
                }
            });
        }
    }

    request.send(data);
});