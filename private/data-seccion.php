<?php 
    if (isset($_POST['munsecc'])){

        error_reporting(0);
        require 'database.php';
        header('Content-type: application/json; charset=utf-8');

        $pdo = Database::connect();
        $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $q = $pdo->prepare('SELECT A.*, B.META, B.AVANCE, B.PENDIENTE, B.PORCENTAJE FROM (SELECT MUNICIPO, CAT_SECC.SECCION, SUM(LN) AS LISTADO FROM CAT_SECC WHERE MUNICIPO LIKE ? GROUP BY MUNICIPO, SECCION) A LEFT JOIN (SELECT MUNICIPO, CAT_SECC.SECCION, COUNT(*) AS META, SUM(ESTATUS) AS AVANCE, (COUNT(*) - SUM(ESTATUS)) AS PENDIENTE, ROUND(((SUM(ESTATUS) * 100) / COUNT(*)),2) AS PORCENTAJE FROM CAT_SECC INNER JOIN ASISTENTES WHERE CAT_SECC.SECCION = ASISTENTES.SECCION AND MUNICIPO LIKE ? GROUP BY MUNICIPO, CAT_SECC.SECCION ORDER BY MUNICIPO) B ON A.SECCION = B.SECCION ORDER BY SECCION');
        $q->execute(array($_POST["munsecc"],$_POST["munsecc"]));
        $data = $q->fetchAll(PDO::FETCH_ASSOC);
        Database::disconnect();

        $total_listado = 0;
        $total_meta = 0;
        $total_avance = 0;
        $total_pendiente = 0;
        $total_porcentaje = 0;
        $datos = [];
        $response = array();

        foreach($data as $row){
            $usuario = [
                'municipio' => $row['MUNICIPO'],
                'seccion' => $row['SECCION'],
                'listado' => number_format((int)$row['LISTADO']),
                'meta' => number_format((int)$row['META']),
                'avance' => number_format((int)$row['AVANCE']),
                'pendiente' => number_format((int)$row['PENDIENTE']),
                'porcentaje' => $row['PORCENTAJE'],
            ];
            
            $total_listado += (int)$row['LISTADO'];
            $total_meta += (int)$row['META'];
            $total_avance += (int)$row['AVANCE'];
            $total_pendiente += (int)$row['PENDIENTE'];
            $total_porcentaje += $row['PORCENTAJE'];
    
            array_push($datos,$usuario);
        }

        $porcentaje_final = ($total_avance * 100) / $total_meta;

    $totales = [
        'total_listado' => number_format($total_listado),
        'total_meta' => number_format($total_meta),
        'total_avance' => number_format($total_avance),
        'total_pendiente' => number_format($total_pendiente),
        'porcentaje_final' => round($porcentaje_final,2),
    ];
    
    $response["municipios"] = $datos;
    $response["totales"] = $totales;
    $response["avance"] = $total_avance;
    $response["pendiente"] = $total_pendiente;
    $response["success"] = "OK";
    echo json_encode($response);
    } else{
        header ("Location: /dashboard.php");
    }
?>