<?php
    if ($_SERVER["REQUEST_METHOD"] == "POST"){
        error_reporting(0);
        require 'database.php';
        require 'databaseA.php';
        /* require 'databaseG.php'; */
        header('Content-type: application/json; charset=utf-8');

        $response = array();

        $pdo = Database::connect();
        $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $q = $pdo->prepare("SELECT count(*) AS meta, SUM(estatus) AS avance, (count(*) - SUM(estatus)) AS pendiente, ((SUM(estatus)/ count(*))*100) AS porcentaje FROM promovidos");
        $q->execute();
        $data = $q->fetch(PDO::FETCH_ASSOC);
        Database::disconnect();

        $protectora = [
            'area' => 'LA PROTECTORA',
            'meta' => number_format((int)$data['meta']),
            'avance' => number_format((int)$data['avance']),
            'pendiente' => number_format((int)$data['pendiente']),
            'porcentaje' => round((float)$data['porcentaje'],2)."%",
            'total_avance' => (int)$data['avance'],
            'total_pendiente' => (int)$data['pendiente']
        ];

        $pdoA = DatabaseA::connect();
        $pdoA->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $qA = $pdoA->prepare("SELECT count(*) AS meta, SUM(estatus) AS avance, (count(*) - SUM(estatus)) AS pendiente, ((SUM(estatus)/ count(*))*100) AS porcentaje FROM promovidos");
        $qA->execute();
        $dataA = $qA->fetch(PDO::FETCH_ASSOC);
        DatabaseA::disconnect();

        $amigos = [
            'area' => 'AMIGOS',
            'meta' => number_format((int)$dataA['meta']),
            'avance' => number_format((int)$dataA['avance']),
            'pendiente' => number_format((int)$dataA['pendiente']),
            'porcentaje' => round((float)$dataA['porcentaje'],2)."%",
            'total_avance' => (int)$dataA['avance'],
            'total_pendiente' => (int)$dataA['pendiente']
        ];

        $response["protectora"] = $protectora;
        $response["amigos"] = $amigos;
        $response["success"] = "OK";
        echo json_encode($response);

        /* $pdoG = DatabaseG::connect();
        $pdoG->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $qG = $pdoG->prepare("SELECT count(*) AS meta, SUM(estatus) AS avance, (count(*)-SUM(estatus)) AS PENDIENTE, ((SUM(estatus)/ count(*))*100) AS PORCENTAJE FROM promovidos");
        $qG->execute();
        $dataG = $qG->fetchAll(PDO::FETCH_ASSOC);
        DatabaseG::disconnect(); */

    }
?>