<?php 
    if (isset($_POST['area'])){

        error_reporting(0);
        require 'database.php';
        header('Content-type: application/json; charset=utf-8');

        $pdo = Database::connect();
        $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $q = $pdo->prepare("SELECT IFNULL(A.id_promotor,B.id_promotor) as id_promotor,IFNULL(A.NOMBRE,B.NOMBRE) AS NOMBRE,(IFNULL(A.sin_asistencia,0)+IFNULL(B.con_asistencia,0)) AS TOTAL,IFNULL(B.con_asistencia,0) AS AVANCE,IFNULL(A.sin_asistencia,0) AS PENDIENTE FROM (SELECT A.id_promotor,CONCAT(A.nombre,' ',A.apellido1,' ',A.apellido2) as NOMBRE,count(*) as sin_asistencia FROM promotor A, promovidos B WHERE A.id_promotor=B.id_promotor AND B.estatus=0 AND A.id_area=? GROUP BY A.id_promotor) A RIGHT JOIN (SELECT A.id_promotor,CONCAT(A.nombre,' ',A.apellido1,' ',A.apellido2) as NOMBRE,count(*) as con_asistencia FROM promotor A, promovidos B WHERE A.id_promotor=B.id_promotor AND B.estatus=1 AND A.id_area=? GROUP BY A.id_promotor) B ON A.id_promotor=B.id_promotor UNION SELECT IFNULL(A.id_promotor,B.id_promotor) as id_promotor,IFNULL(A.NOMBRE,B.NOMBRE) AS NOMBRE,(IFNULL(A.sin_asistencia,0)+IFNULL(B.con_asistencia,0)) AS TOTAL,IFNULL(B.con_asistencia,0) AS AVANCE,IFNULL(A.sin_asistencia,0) AS PENDIENTE FROM (SELECT A.id_promotor,CONCAT(A.nombre,' ',A.apellido1,' ',A.apellido2) as NOMBRE,count(*) as sin_asistencia FROM promotor A, promovidos B WHERE A.id_promotor=B.id_promotor AND B.estatus=0 AND A.id_area=? GROUP BY A.id_promotor) A LEFT JOIN (SELECT A.id_promotor,CONCAT(A.nombre,' ',A.apellido1,' ',A.apellido2) as NOMBRE,count(*) as con_asistencia FROM promotor A, promovidos B WHERE A.id_promotor=B.id_promotor AND B.estatus=1 AND A.id_area=? GROUP BY A.id_promotor) B ON A.id_promotor=B.id_promotor ORDER BY NOMBRE ASC");
        $q->execute(array($_POST["area"],$_POST["area"],$_POST["area"],$_POST["area"]));
        $data = $q->fetchAll(PDO::FETCH_ASSOC);
        Database::disconnect();

        $total_listado = 0;
        $total_avance = 0;
        $total_pendiente = 0;
        $datos = [];
        $response = array();

        foreach($data as $row){
            $porcentaje = round(((int)$row['AVANCE'] * 100) / (int)$row['TOTAL'],2);
            if($porcentaje === 100 || $porcentaje_final === 0){
                $porcentaje = number_format($porcentaje);
            } else{
                $porcentaje = number_format($porcentaje,2);
            }
            $promotores = [
                'promotor' => $row['NOMBRE'],
                'listado' => number_format((int)$row['TOTAL']),
                'avance' => number_format((int)$row['AVANCE']),
                'pendiente' => number_format((int)$row['PENDIENTE']),
                'porcentaje' => $porcentaje
            ];
            
            $total_listado += (int)$row['TOTAL'];
            $total_avance += (int)$row['AVANCE'];
            $total_pendiente += (int)$row['PENDIENTE'];
    
            array_push($datos,$promotores);
        }

        $porcentaje_final = round(($total_avance * 100) / $total_listado,2);
        if($porcentaje_final === 100 || $porcentaje_final === 0){
            $porcentaje_final = number_format($porcentaje_final);
        } else{
            $porcentaje_final = number_format($porcentaje_final,2);
        }
    $totales = [
        'total_listado' => number_format($total_listado),
        'total_avance' => number_format($total_avance),
        'total_pendiente' => number_format($total_pendiente),
        'total_porcentaje' => $porcentaje_final
    ];
    
    $response["promotores"] = $datos;
    $response["totales"] = $totales;
    $response["avance"] = $total_avance;
    $response["pendiente"] = $total_pendiente;
    $response["success"] = "OK";
    echo json_encode($response);
    } else{
        header ("Location: /dashboard.php");
    }
?>