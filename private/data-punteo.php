<?php 

    error_reporting(0);
    require 'database.php';
    header('Content-type: application/json; charset=utf-8');

    if (isset($_POST['id'])){

        $pdo = Database::connect();
        $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $q = $pdo->prepare('SELECT A.id_promotor, A.nombre, A.apellido1, A.apellido2,(IFNULL(A.TOTALES,0) - IFNULL(B.AVANCE,0)) AS pendientes FROM (SELECT A.*, COUNT(B.id_promovido) AS TOTALES FROM promotor A, promovidos B WHERE A.id_promotor = B.id_promotor AND A.id_usuario = ? GROUP BY A.id_promotor) A LEFT JOIN (SELECT A.*, COUNT(B.id_promovido) AS AVANCE FROM promotor A, promovidos B WHERE A.id_promotor = B.id_promotor AND B.estatus = 1 AND A.id_usuario = ? GROUP BY A.id_promotor) B ON A.id_promotor = B.id_promotor ORDER BY A.nombre, A.apellido1, A.apellido2, pendientes');
        $q->execute(array($_POST["id"],$_POST["id"]));
        $data = $q->fetchAll(PDO::FETCH_ASSOC);
        Database::disconnect();

        $datos = [];

        foreach($data as $row){
            $promotor = [
                'id' => $row['id_promotor'],
                'nombre' => $row['nombre'].' '.$row['apellido1'].' '.$row['apellido2'],
                'pendientes' => $row['pendientes'],
                'telefono' => $row['telefono']
            ];
    
            array_push($datos,$promotor);
        }
    
        echo json_encode($datos);
    } else if(isset($_POST['promotor'])){
        $pdo = Database::connect();
        $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $q = $pdo->prepare('SELECT * FROM `promovidos` WHERE id_promotor = ? ORDER BY nombre, apellido1, apellido2');
        $q->execute(array($_POST["promotor"]));
        $data = $q->fetchAll(PDO::FETCH_ASSOC);
        Database::disconnect();

        $datos = [];

        foreach($data as $row){
            $promovido= [
                'id' => $row['id_promovido'],
                'nombre' => $row['nombre'].' '.$row['apellido1'].' '.$row['apellido2'],
                'edad' => $row['edad'],
                'estatus' => $row['estatus']
            ];
    
            array_push($datos,$promovido);
        }
    
        echo json_encode($datos);
    } else if(isset($_POST['data'])){

        $punteos = json_decode($_POST['data'],true);
        $st = false;
        $response = [];

        foreach($punteos as $row){
            $pdo = Database::connect();
            $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $q = $pdo->prepare('UPDATE promovidos SET estatus=? WHERE id_promovido=?');
            $q->execute(array($row["estatus"],$row["cve"]));
            Database::disconnect();
            if($q){
                $st = true;
            } else{
                $st = false;
            }
        }

        if($st){
            $response["success"] = "OK";
            echo json_encode($response);
        } else{
            $response["error"] = "No se pudieron actualizar todos los registros";
            echo json_encode($response);
        }

    } else {
        header ("Location: /dashboard.php");
    }
?>