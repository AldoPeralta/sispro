<?php
    error_reporting(0);
    require 'database.php';
    header('Content-type: application/json; charset=utf-8');
    session_start();
    if(isset($_POST["usuario"]) && isset($_POST["password"])){
        $datos = array();
        $pdo = Database::connect();
        $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $q = $pdo->prepare('SELECT A.id_usuario AS id , A.usuario, A.password, A.id_nivel AS nivel, A.id_area, B.area FROM usuario A, areas B WHERE usuario = ? AND password = ? AND A.id_area = B.id_area');
        $q->execute(array($_POST["usuario"], $_POST["password"]));
        $data = $q->fetch(PDO::FETCH_ASSOC);
        Database::disconnect();
        if(!empty($data['usuario'])){
            $_SESSION["auth"] = "access";
            $_SESSION["user"] = $data['usuario'];
            $_SESSION["id"] = $data['id'];
            $_SESSION["nivel"] = $data['nivel'];
            $datos['success'] = 'OK';
            $datos['id'] = $data['id'];
            $datos['area'] = $data['id_area'];
            $datos['nombre'] = $data['area'];
            echo json_encode($datos);
        } else{
            $datos['nouser'] = 'OK';
            echo json_encode($datos);
        }
    }
?>