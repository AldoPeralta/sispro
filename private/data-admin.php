<?php 
    if ($_SERVER["REQUEST_METHOD"] == "POST"){

        error_reporting(0);
        require 'database.php';
        header('Content-type: application/json; charset=utf-8');

        $pdo = Database::connect();
        $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $q = $pdo->prepare("SELECT A.id_area,B.area,A.TOTAL,A.AVANCE,A.PENDIENTE FROM (SELECT IFNULL(A.id_area,B.id_area) as id_area,(IFNULL(A.sin_asistencia,0)+IFNULL(B.con_asistencia,0)) AS TOTAL,IFNULL(B.con_asistencia,0) AS AVANCE,IFNULL(A.sin_asistencia,0) AS PENDIENTE FROM (SELECT A.id_area,count(*) as sin_asistencia FROM promotor A, promovidos B WHERE A.id_promotor=B.id_promotor AND B.estatus=0 GROUP BY A.id_area) A RIGHT JOIN (SELECT A.id_area,count(*) as con_asistencia FROM promotor A, promovidos B WHERE A.id_promotor=B.id_promotor AND B.estatus=1 GROUP BY A.id_area) B ON A.id_area=B.id_area UNION SELECT IFNULL(A.id_area,B.id_area) as id_area,(IFNULL(A.sin_asistencia,0)+IFNULL(B.con_asistencia,0)) AS TOTAL,IFNULL(B.con_asistencia,0) AS AVANCE,IFNULL(A.sin_asistencia,0) AS PENDIENTE FROM (SELECT A.id_area,count(*) as sin_asistencia FROM promotor A, promovidos B WHERE A.id_promotor=B.id_promotor AND B.estatus=0 GROUP BY A.id_area) A LEFT JOIN (SELECT A.id_area,count(*) as con_asistencia FROM promotor A, promovidos B WHERE A.id_promotor=B.id_promotor AND B.estatus=1 GROUP BY A.id_area) B ON A.id_area=B.id_area) A, areas B WHERE A.id_area=B.id_area");
        $q->execute();
        $data = $q->fetchAll(PDO::FETCH_ASSOC);
        Database::disconnect();

        $total_listado = 0;
        $total_avance = 0;
        $total_pendiente = 0;
        $datos = [];
        $response = array();

        foreach($data as $row){
            $porcentaje = round(($row['AVANCE'] * 100) / $row['TOTAL'],2);
            if($porcentaje === 100 || $porcentaje_final === 0){
                $porcentaje = number_format($porcentaje);
            } else{
                $porcentaje = number_format($porcentaje,2);
            }
            $areas = [
                'id' => $row['id_area'],
                'area' => $row['area'],
                'listado' => number_format((int)$row['TOTAL']),
                'avance' => number_format((int)$row['AVANCE']),
                'pendiente' => number_format((int)$row['PENDIENTE']),
                'porcentaje' => $porcentaje
            ];
            
            $total_listado += (int)$row['TOTAL'];
            $total_avance += (int)$row['AVANCE'];
            $total_pendiente += (int)$row['PENDIENTE'];
    
            array_push($datos,$areas);
        }

        $porcentaje_final = round(($total_avance * 100) / $total_listado,2);
        if($porcentaje_final === 100 || $porcentaje_final === 0){
            $porcentaje_final = number_format($porcentaje_final);
        } else{
            $porcentaje_final = number_format($porcentaje_final,2);
        }
    $totales = [
        'total_listado' => number_format($total_listado),
        'total_avance' => number_format($total_avance),
        'total_pendiente' => number_format($total_pendiente),
        'total_porcentaje' => $porcentaje_final
    ];
    
    $response["areas"] = $datos;
    $response["totales"] = $totales;
    $response["avance"] = $total_avance;
    $response["pendiente"] = $total_pendiente;
    $response["success"] = "OK";
    echo json_encode($response);
    } else{
        header ("Location: /dashboard.php");
    }
?>