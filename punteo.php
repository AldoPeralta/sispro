<div class="row justify-content-center" style="margin-bottom:50px;">
    <div class="col text-center">
        <img class="img-header" src="./images/municipios/TODO EL ESTADO.png" alt="Mapa Hidalgo">
    </div>
    <div class="col-6 align-self-center txt">
        <h2 class="text-center subtitle">
            <b>PUNTEO POR MUNICIPIO</b>
        </h2>
    </div>
    <div class="col text-center">
        <img class="img-header" src="./images/pri.png" alt="NIP">
    </div>
</div>

<div class="row justify-content-center mb-3">
    <div class="col-12 col-md-8 col-lg-6 text-center">
        <h2 class="text-center subtitle1">
            Punteos por cargar: <span id="punteos" class="badge badge-warning" style="color:#fff;">0</span>
        </h2>
        <button class="btn btn-primary" id="btnCargar">
            <b>Actualizar Datos</b>
        </button>
    </div>
</div>

<div class="row justify-content-center">
    <div class="col-12 col-md-11">
        <div class="contenedor">
            <div class="selectbox">
                <div class="select" id="select">
                    <div class="contenido-select">
                        <h4 class="tituloC">Selecciona un promotor</h4>
                        <p class="descripcion">...</p>
                    </div>
                    <i class="fas fa-angle-down"></i>
                </div>
                <div class="opciones" id="opciones"></div>
            </div>
        </div>
    </div>
    <div class="col-12 col-lg-8">
        <div class="accordion" id="accordionExample" style="margin-bottom:105px !important;">
            <div class="card">
                <div class="card-header" id="headNoPromov">
                    <h2 class="mb-0">
                        <button class="btn btn-block text-center" id="btnNoPromov" type="button" data-toggle="collapse" data-target="#collapseNoPromov" aria-expanded="true" aria-controls="collapseNoPromov">
                            <b>PENDIENTES</b> <i class="fas fa-arrow-down"></i>
                        </button>
                    </h2>
                </div>

                <div id="collapseNoPromov" class="collapse" aria-labelledby="headNoPromov" data-parent="#accordionExample">
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-hover">
                                <thead class="thead-dark">
                                    <tr class="text-center">
                                        <th>NOMBRE</th>
                                        <th>EDAD</th>
                                        <th>ASISTIÓ</th>
                                    </tr>
                                </thead>
                                <tbody id="tablenopromov">
                                    
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card">
                <div class="card-header" id="headPromov">
                    <h2 class="mb-0">
                        <button class="btn btn-block text-center" id="btnPromov" type="button" data-toggle="collapse" data-target="#collapsePromov" aria-expanded="false" aria-controls="collapsePromov">
                            <b>ASISTENCIA</b> <i class="fas fa-arrow-down"></i>
                        </button>
                    </h2>
                </div>

                <div id="collapsePromov" class="collapse" aria-labelledby="headPromov" data-parent="#accordionExample">
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-hover">
                                <thead class="thead-dark">
                                    <tr class="text-center">
                                        <th>NOMBRE</th>
                                        <th>EDAD</th>
                                        <th>ASISTIÓ</th>
                                    </tr>
                                </thead>
                                <tbody id="tablepromov">
                                
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="./js/sise/data-punteo-new.js"></script>