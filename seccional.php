<?php require 'onHead.php';?>

    <div class="container">
        <div class="row justify-content-center mb-3">
            <div class="col-12 col-lg-6">
                <h1 class="text-center">
                    BIENVENIDO 
                    <span class="badge badge-danger">
                        <?php echo $_SESSION["user"];?>
                    </span>
                </h1>
            </div>
        </div>

        <div class="row justify-content-center" style="margin-bottom:50px;">
            <div class="col-12 col-lg-3 text-center">
                <img src="./images/municipios/<?php echo $_POST['munsecc'];?>.png" width="65%" alt="Mapa Hidalgo">
            </div>
            <div class="col-12 col-lg-6 align-self-center">
                <h2 class="text-center">
                    Avance por seccion
                </h2>
                <h4 class="text-center" id="titulo">
                    <?php echo $_POST['munsecc'];?>
                </h4>
            </div>
            <div class="col-12 col-lg-3 text-center">
                <img src="./images/pri.png" width="65%" alt="Mapa Hidalgo">
            </div>
        </div>

        <div class="row justify-content-center" style="margin-bottom:55px;">
            <div class="col-12 col-lg-10">
                <div class="table-responsive">
                    <table class="table table-hover">
                        <thead class="thead-dark">
                            <tr>
                                <th>MUNICIPIO</th>
                                <th>SECCION</th>
                                <th>LISTADO</th>
                                <th>META</th>
                                <th>AVANCE</th>
                                <th>PENDIENTE</th>
                                <th>PORCENTAJE</th>
                            </tr>
                        </thead>
                        <tbody id="tablesecc">
                            
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

        <div class="row justify-content-center" style="margin-bottom:85px;">
            <div class="col-12 col-md-10 col-lg-8">
                <canvas id="seccChart" class="w-100"></canvas>
            </div>
        </div>
    </div>

    <script src="./js/data-seccion.js"></script>

<?php require 'onBody.php';?>