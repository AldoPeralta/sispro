<div class="row justify-content-center" style="margin-bottom:50px;">
    <div class="col text-center">
        <img class="img-header" src="./images/municipios/TODO EL ESTADO.png" alt="Mapa Hidalgo">
    </div>
    <div class="col-6 align-self-center txt">
        <h2 class="text-center subtitle">
            REPORTE GENERAL
        </h2>
    </div>
    <div class="col text-center">
        <img class="img-header" src="./images/pri.png" alt="NIP">
    </div>
</div>

<div class="row justify-content-center" style="margin-bottom:55px;">
    <div class="col-12 col-lg-10">
        <div class="table-responsive">
            <table class="table table-hover text-center">
                <thead class="thead-dark">
                    <tr>
                        <th>AREA</th>
                        <th>META</th>
                        <th>AVANCE</th>
                        <th>PENDIENTE</th>
                        <th>PORCENTAJE</th>
                    </tr>
                </thead>
                <tbody id="tableadmin">
                    
                </tbody>
            </table>
        </div>
    </div>
</div>

<div class="row justify-content-center" style="margin-bottom:85px;">
    <div class="col-12 col-md-10 col-lg-8">
        <canvas id="adminChart" class="w-100"></canvas>
    </div>
</div>

<script src="./js/sise/data-admin.js"></script>