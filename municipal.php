
<div class="row justify-content-center" style="margin-bottom:50px;">
    <div class="col text-center">
        <img class="img-header" src="./images/municipios/TODO EL ESTADO.png" alt="Mapa Hidalgo">
    </div>
    <div class="col-6 align-self-center txt">
        <h2 class="text-center subtitle">
            REPORTE GENERAL
        </h2>
        <h3 class="text-center subtitle" id="area"></h3>
    </div>
    <div class="col text-center">
        <img class="img-header" src="./images/pri.png" alt="NIP">
    </div>
</div>

<div class="row justify-content-center" style="margin-bottom:55px;">
    <div class="col-12 col-lg-10">
        <div class="table-responsive">
            <table class="table table-hover text-center">
                <thead class="thead-dark">
                    <tr>
                        <th>MUNICIPIO</th>
                        <th>META</th>
                        <th>AVANCE</th>
                        <th>PENDIENTE</th>
                        <th>%</th>
                    </tr>
                </thead>
                <tbody id="tablemunicipio"></tbody>
            </table>
        </div>
    </div>
</div>

<div class="row justify-content-center" style="margin-top:35px;margin-bottom:65px;">
    <div class="col-12 col-md-10 col-lg-12">
        <canvas id="municipalChart" class="w-100"></canvas>
    </div>
</div>

<script src="./js/sise/data-municipal.js"></script>