<?php
    session_start();
    if (isset($_SESSION['auth'])){
  	    header ("Location: /dashboard.php");
    }
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="./css/signin.css">
        <link rel="icon" href="./favicon.ico">
        <title>SISPRO</title>
    </head>
    <body>
        <main>
            <form action="" class="formulario" id="formulario">
                <div class="formulario__imagen">
                    <img src="./images/sispro.png" alt="">
                </div>
                <div class="formulario__grupo" id="grupo__usuario">
                    <label for="usuario" class="formulario__label">Usuario</label>
                    <div class="formulario__grupo-input">
                        <input type="text" class="formulario__input" name="usuario" id="usuario" placeholder="Ingresa tu usuario">
                        <i class="formulario__validacion-estado fas fa-times-circle"></i>
                    </div>
                    <p class="formulario__input-error">
                        El nombre de usuario solo debe contener letras mayúsculas, minúsculas o números.
                    </p>
                </div>
                <div class="formulario__grupo" id="grupo__password">
                    <label for="password" class="formulario__label">Contraseña</label>
                    <div class="formulario__grupo-input">
                        <input type="password" class="formulario__input" name="password" id="password" placeholder="Ingresa tu contraseña">
                        <i class="formulario__validacion-estado fas fa-times-circle"></i>
                    </div>
                    <p class="formulario__input-error">
                        La contraseña tiene que ser de 6 a 16 dígitos.
                    </p>
                </div>
                <div class="formulario__grupo formulario__grupo-btn-enviar">
                    <button type="submit" class="formulario__btn" id="senderbtn">Ingresar</button>
                </div>
            </form>
        </main>

        <script src="https://cdn.jsdelivr.net/npm/sweetalert2@10"></script>
        <script src="https://kit.fontawesome.com/2c36e9b7b1.js" crossorigin="anonymous"></script>
	    <script src="./js/sise/s-login.js"></script>
    </body>
</html>