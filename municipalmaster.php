<?php require 'onHead.php';?>

    <div class="container">
        <div class="row justify-content-center mb-3">
            <div class="col-12 col-lg-6">
                <h1 class="text-center">
                    BIENVENIDO 
                    <span class="badge badge-danger">
                        <?php echo $_SESSION["user"];?>
                    </span>
                </h1>
            </div>
        </div>
        
        <div class="row justify-content-center" style="margin-bottom:50px;">
            <div class="col text-center">
                <img class="img-header" src="./images/municipios/TODO EL ESTADO.png" alt="Mapa Hidalgo">
            </div>
            <div class="col-6 align-self-center txt">
                <h2 class="text-center subtitle">
                    REPORTE POR MUNICIPIO
                </h2>
                <h3 class="text-center subtitle" id="area"></h3>
            </div>
            <div class="col text-center">
                <img class="img-header" src="./images/pri.png" alt="NIP">
            </div>
        </div>

        <div class="row justify-content-center" style="margin-bottom:55px;">
            <div class="col-12 col-lg-10">
                <div class="table-responsive">
                    <table class="table table-hover text-center">
                        <thead class="thead-dark">
                            <tr>
                                <th>MUNICIPIO</th>
                                <th>META</th>
                                <th>AVANCE</th>
                                <th>PENDIENTE</th>
                                <th>%</th>
                            </tr>
                        </thead>
                        <tbody id="tablemunmaster"></tbody>
                    </table>
                </div>
            </div>
        </div>

        <div class="row justify-content-center" style="margin-top:35px;margin-bottom:65px;">
            <div class="col-12 col-md-10 col-lg-8">
                <canvas id="municipalMasterChart" style="width:100% !important; height: 350px !important;"></canvas>
            </div>
        </div>

    </div>

<script src="./js/sise/data-mun-master.js"></script>

<?php require 'onBody.php';?>