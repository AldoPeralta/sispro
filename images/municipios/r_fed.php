<?php 
include 'mysql_connect_general.php';

$total = $conexion->prepare('SELECT CAB_FED, DIS_FED, COUNT(*) AS META, SUM(ESTATUS) AS AVANCE, (COUNT(*) - SUM(ESTATUS)) AS PENDIENTE, ROUND(((SUM(ESTATUS) * 100) / COUNT(*)),2) AS PORCENTAJE FROM CAT_SECC INNER JOIN ASISTENTES WHERE CAT_SECC.SECCION = ASISTENTES.SECCION GROUP BY DIS_FED, CAB_FED ORDER BY DIS_FED;');

	$total->execute();
	$rst = $total->fetchAll();
	$contenido1 = '';
	$total_meta1 = 0;
	$total_avance1 = 0;
	$total_pendiente1 = 0;
	$municipios1 = 0;
	$total_porcentaje1 = 0;

	foreach ($rst as $row) {
		$contenido1 .= '<tr>
						    <td><form action="r_mun.php" method="POST"><input type="submit" name="FEDERAL" value="'.$row['DIS_FED'].'" style="width: 50px; height: 25px; font-size: 13px;"></form></td>
						    <td>'.$row['CAB_FED'].'</td>
						    <td>'.number_format($row['META']).'</td>
						    <td>'.number_format($row['AVANCE']).'</td>
						    <td>'.number_format($row['PENDIENTE']).'</td>
						    <td>'.$row['PORCENTAJE'].'%</td>
						</tr>';

		$total_meta1 += (int)$row['META'];
		$total_avance1 += (int)$row['AVANCE'];
		$total_pendiente1 += (int)$row['PENDIENTE'];
		$municipios1 += 1;
		$total_porcentaje1 += $row['PORCENTAJE'];
	}

	$total_porc1 = ($total_porcentaje1 * 100) / ($municipios1 * 100);
?>
<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
	<title>Menu</title>
	<link rel="stylesheet" href="css/bootstrap.min.css">
	<link href="https://fonts.googleapis.com/css?family=Open+Sans:400,700|Roboto:300,400,500" rel="stylesheet">
	<link rel="stylesheet" href="css/fontello.css">
	<link rel="stylesheet" href="css/estilos-dash.css">
	<link rel="shortcut icon" href="favicon.ico" type="image/x-icon">
    <link rel="icon" href="/favicon.ico" type="image/x-icon">
</head>
<script>
		function recarga(){
			location.href=location.href
		}
		setInterval('recarga()',10000)
	</script>
<body style="background-color: white;">
	<div class="container">
			<main class="">

				<!-- DISTRITO FEDERAL I -->
				<div class="row justify-content-center">
					<div class="col-sm-12 col-md-2 col-lg-1">
						<img src="img/pri.png" width="93px" height="93px">
					</div>
					<div class="col-sm-12 col-md-4 col-lg-10">
						<h1 style="text-align: center;">AVANCE GENERAL</h1>
						<h5 style="text-align: center;">DISTRITOS FEDERALES</h5>
					</div>
					<div class="col-sm-12 col-md-2 col-lg-1">
						<img src="img/hidalgo.png" width="93px" height="93px">
					</div>
				</div>

				<div class="row justify-content-center">
					<div class="col-12">
								
								<div class="table-responsive">
						            <table class="table table-striped table-sm table-hover" style="font-size: 13px; text-align: center;">
						              <thead>
						                <tr>
						                  <th>DISTRITO FEDERAL</th>
						                  <th>CABECERA</th>
						                  <th>META</th>
						                  <th>AVANCE</th>
						                  <th>PENDIENTES</th>
						                  <th>PORCENTAJE</th>
						                </tr>
						              </thead>
						              <tbody>
						                
						                <?php 
						                	echo $contenido1;
						                ?>

						                <tr>
										    <td></td>
										    <td><b>TOTAL:</b></td>
										    <td><b><?php echo number_format($total_meta1); ?></b></td>
										    <td><b><?php echo number_format($total_avance1); ?></b></td>
										    <td><b><?php echo number_format($total_pendiente1); ?></b></td>
										    <td><b><?php echo round($total_porc1,2); ?>%</b></td>
										</tr>
						                
						              </tbody>
						            </table>
						</div>
					</div>
				</div>

				<div class="row justify-content-center">
				
					<div class="col-12" id="container1" style="height: 325px;"></div>

				</div>


			</main>
	</div>

	
	<script src="hc/code/highcharts.js"></script>
	<script src="hc/code/modules/exporting.js"></script>
	<script src="hc/code/modules/export-data.js"></script>
	<script src="js/jquery-3.2.1.min.js"></script>
	<script src="js/popper.min.js"></script>
	<script src="js/bootstrap.min.js"></script>

	<script type="text/javascript">

			Highcharts.setOptions({
				colors: ['#00c853','#d50000']
			});

			Highcharts.chart('container1', {
			    chart: {
			        plotBackgroundColor: null,
			        plotBorderWidth: null,
			        plotShadow: false,
			        type: 'pie'
			    },
			    title: {
			        text: ''
			    },
			    tooltip: {
			        pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
			    },
			    plotOptions: {
			        pie: {
			            allowPointSelect: true,
			            cursor: 'pointer',
			            dataLabels: {
			                enabled: true,
			                format: '<b>{point.name}</b>: {point.percentage:.1f} % , {point.y}',
			                style: {
			                    color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
			                }
			            }
			        }
			    },
			    series: [{
			        name: 'Alumnas',
			        colorByPoint: true,
			        data: [{
			            name: 'Listados',
			            y: <?php echo $total_avance1; ?>,
			            sliced: true,
			            selected: true
			        }, {
			            name: 'No listados',
			            y: <?php echo $total_pendiente1; ?>
			        }]
			    }]
			});
		</script>

	
</body>
</html>